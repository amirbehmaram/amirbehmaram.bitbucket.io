# USAC cutup

Welcome to USAC!

This was built using Jekyll, a static site generator that supports SASS and includes Liquid, a helpful templating language.

Follow the Jekyll installation guide here if you don't already have Jekyll: https://jekyllrb.com/docs/installation/

Once Jekyll is installed navigate into the project folder and run ```jekyll serve --livereload``` to start the site up.
Go to ```http://localhost:4000/``` to view the site.

## Code Structure

Directories:

- _includes

    - components: Smaller chunks of HTML that can be called repeatedly within sections.

    - global: HTML for the header, footer, and menu live here

    - sections: HTML for bigger "blocks" of the site live here. These should for the most part always be div's whose topmost element is a row so we can just call these from within a container and not have any issues.

- _layouts

    - The html files in the root of the directory should specify which layout they are using at the top of their files `layout: default`. The layout wraps whatever html file calls it.

- assets

    - bootstrap

        - Boostrap SCSS files. We include certain scss files from the custom folder within the bootstrap.scss file to override bootstrap styles

        - Current folder size is 430KB

    - custom

        - Custom SASS files to help style things that Bootstrap can't achieve

    - foundation

        - Foundation SCSS files. The files included in this directory are only for the drilldown menu. No other foundation files are included in here.

        - Current folder size is 98KB

    - images

    - js: Currently only 1 app.js file. This may be broken up if it gets large, but it's under 50 lines currently so it's fine for now.

    - svgs: used for the social icons and USAC logos

## Important Notes

#### Updating Bootstrap
If a new bootstrap version comes out updating to it is pretty straightforward.

- The first and most important step is to go into the `assets/css/bootstrap` directory and create a backup of the `bootstrap.scss` file.

    - This file is where we import out bootstrap overrides/addition classes.

- Navigate to https://getbootstrap.com/ and click the `Download` button to download the latest source files for bootstrap

- Download the Source Files

- Open the .zip and go into the `scss` folder.

- Copy the contents of that folder in to the `assets/css/bootstrap` directory within the project.

- Go to the new `bootstrap.scss` file in our project and copy over anything that is between `/* Copy me on updates */` & `/* ****************** */` within the backup `bootstrap.scss` file.

    - Ensure the variable overrides are at the top and style overrides are at the bottom.

- Finally, go back to https://getbootstrap.com/ or your preferred CDN and get the latest versions of JS, Popper.js, and jQuery and put those in teh bottom of the site footer.

#### Current list of Bootstrap Components we are overriding
- Buttons

    - Primary, Secondary, Tertiary colors and hover states will be following the style guide.

    - Additional non-bootstrap classes will be added here as well.

        - ex: adding the class btn-trailing-carat to a btn class will append a '>' to the button's text

- Cards

    - Card title & body adjustments to match our comps

    - Additional non-bootstrap classes will be added here as well.

        - ex: adding a div with class `card-blog-top-banner` after the `card-img-top` element will add the opaque black banner over the top image

#### Site Menu:
The Menu in the cutup may look a little daunting but it's just comprised of nested list elements

The basic structure of the menu is as follow:
```
<ul>
    <li>
        <a href="#">Menu Item</a>
        <!--
        If this menu item has a sub menu then the
        items below this are needed.
        -->
        <a><i class="Font awesome icon"></i></a>
        <ul>
            <li>
                <div>Denotes current menu location</div>
            </li>
            <li>
                <a href="#">Menu Item</a>
                <!--
                    If this menu item has a sub menu then the
                    items below this are needed.
                -->
                <a><i class="Font awesome icon"></i></a>
                <ul>etc...</ul>
            </li>
            .
            .
            .
            As many list items as you want
        </ul>
    </li>
    .
    .
    .
    As many list items as you want
</ul>
```
Foundation will handle the rest assuming the correct classes are on the list tags and anchor tags. Foundation doesn't attach click events to buttons within the drilldown though so everything had to be an anchor tag.

## Helpful Links

Jekyll Docs: https://jekyllrb.com/docs/

Liquid Docs: https://shopify.github.io/liquid/

Bootstrap 4 Docs: https://getbootstrap.com/docs/4.0/getting-started/introduction/

Foundation Drilldown Menu: https://foundation.zurb.com/sites/docs/drilldown-menu.html
