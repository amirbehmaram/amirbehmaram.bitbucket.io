function usacJS() {
  "use strict";

  /**************************
   * Site Menu Handling
   **************************/
  // Foundation Drilldown creation
  $(document).foundation();

  // Replace back button text with parents name
  $('.custom-back').each(function () {
    var backTxt = $(this).parent().parent().data('currentLocation');
    $(this).text('Back To ' + backTxt);
  });

  // Add the current menu location text to the list
  $('.site-menu-current-location').each(function () {
    var currentLocation = $(this).parent().closest('.is-drilldown-submenu-parent').find('> a').text();
    $(this).text(currentLocation);
  });

  // On main menu button click fire the hideAll method to closes all open elements, and return to the root
  $('.site-menu-main-menu-link').click(function () {
    $('#main-menu').foundation('_hideAll');
  });

  /**
   * Restore any saved href's that foundation strips off elements with sub menus
   * AND then remove click events from the links themselves
  */
  $('.site-menu-link').each(function () {
    var href = $(this).data('savedHref');

    if (href) {
      this.href = href;
    }
  }).off('click');

  // Show mobile menu on hamburger button click
  $('#menu-button').click(function () {
    $('.site').toggleClass('site-show-menu');
  });

  /**************************
   * Location Card menu handling
   **************************/

  /**
   * Set opacity of all card location list items
   *
   * Starting from the bottom going up, each menu item has an opacity of the previous item - 5%
   */

  $($('.card-location-list')).each(function () {
    var opacity = .95;
    $($(this).find('li').get().reverse()).each(function () {
      opacity = opacity - .05;
      $(this).css('opacity', opacity);
    });
  });

  // Card slide up and down animation handling
  $('.card-location-toggle').on('click', function () {
    $(this).parent().prev().find('ul').animate({
      height: 'toggle'
    }, 1000);
  });

  /**************************
   * Carousel Javascript
   **************************/
  $('#cardCarousel').carousel({
    interval: 10000000,
    wrap: true,
    ride: 'carousel',
    touch: true
  });

  $('#imageCarousel').carousel({
    interval: 10000,
    wrap: true,
    ride: 'carousel',
    touch: true
  });

  /**************************
   * Slick Carousel Logic for News and Icon List
   **************************/

  function checkWidth() {
    if ($(window).width() > 1000) {
      $('.slick-carousel-moble').addClass('slick-carousel').removeClass('slick-carousel-mobile');
      $('.icon-list').removeClass('slick-carosel-mobile')
    } else {
      $('.slick-carousel').addClass('slick-carousel-mobile').removeClass('slick-carousel');
      $('.icon-list').addClass('slick-carousel-mobile').removeClass('icon-list');
    }

    console.log('current width', $(window).width());
  }

  checkWidth();

  $('.slick-carousel').slick({
    slidesToShow: 3,
    prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button" style=""><i class="fa fa-chevron-left fa-lg text-muted"></i></button><span class="sr-only">Previous</span>',
    nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style=""><i class="fa fa-chevron-right fa-lg text-muted"></i></button><span class="sr-only">Next</span>'
  });

  $('.slick-carousel-mobile').slick({
    slidesToShow: 2,
    variableWidth: true,
    arrows: false
  });

  $(window).resize(checkWidth());


  /**
    * This is essentially everything needed to make our card carousel:
    * https://www.codeply.com/go/s3I9ivCBYH/bootstrap-4-responsive-carousel-one-at-a-time
    *
    * Some of the variable names were changed to be more descriptive.
    * Basically all this does is on the slide event, it re-appends the element that just slid out
    * of view to the end of our carousel so we get that nice infinite scrolling.
  */
  $('#cardCarousel').on('slide.bs.carousel', function (e) {
    var $e = $(e.relatedTarget);
    var index = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $('.carousel-item').length;

    if (index >= totalItems - (itemsPerSlide - 1)) {
      var item = itemsPerSlide - (totalItems - index);
      for (var i = 0; i < item; i++) {
        // append slides to end
        if (e.direction == "left") {
          $('.carousel-item').eq(i).appendTo('.carousel-inner');
        }
        else {
          $('.carousel-item').eq(0).appendTo('.carousel-inner');
        }
      }
    }
  });

  $('.card-header .accordion-title .btn-link').on('click', function () {
    if (!$(this).parent().parent().hasClass('open')) {
      $('.card-header').removeClass('open');
      $(this).parent().parent().addClass('open');
      $(this).addClass('btn-open');
      $(this).removeClass('btn');
    } else {
      $('.collapse').on('hide.bs.collapse', function () {
        $('.card-header').removeClass('open');
        $('.btn-open').removeClass('btn-open');
        $('.btn-link').addClass('btn')
      })
    }
  });
}

(function ($) {
  $(document).ready(usacJS());
})(jQuery);
